// Copyright (C) 2020 Pieter Penninckx
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
use std::collections::VecDeque;

pub trait Active {
    fn is_active(&self) -> bool;
}

pub struct SelfClearingVecDeque<T>
where
    T: Active,
{
    inner: VecDeque<T>,
}

pub struct SelfClearingVecDequeIterMut<'a, T> {
    inner: std::collections::vec_deque::IterMut<'a, T>,
}

impl<'a, T> Iterator for SelfClearingVecDequeIterMut<'a, T> {
    type Item = &'a mut T;

    fn next(&mut self) -> Option<Self::Item> {
        self.inner.next()
    }
}

pub struct SelfClearingVecDequeIter<'a, T> {
    inner: std::collections::vec_deque::Iter<'a, T>,
}

impl<'a, T> Iterator for SelfClearingVecDequeIter<'a, T> {
    type Item = &'a T;

    fn next(&mut self) -> Option<Self::Item> {
        self.inner.next()
    }
}

impl<T> SelfClearingVecDeque<T>
where
    T: Active,
{
    pub fn with_capacity(capacity: usize) -> Self {
        Self {
            inner: VecDeque::with_capacity(capacity),
        }
    }

    pub fn push(&mut self, data: T) {
        if !data.is_active() {
            return;
        }
        self.inner.retain(T::is_active);
        self.inner.push_back(data);
    }

    pub fn iter_mut(&mut self) -> SelfClearingVecDequeIterMut<T> {
        SelfClearingVecDequeIterMut {
            inner: self.inner.iter_mut(),
        }
    }

    pub fn iter(&self) -> SelfClearingVecDequeIter<T> {
        SelfClearingVecDequeIter {
            inner: self.inner.iter(),
        }
    }

    pub fn len(&self) -> usize {
        self.inner.len()
    }
}
