// Copyright (C) 2020 Pieter Penninckx
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
use std::collections::vec_deque::VecDeque;
use std::fmt::{Debug, Error, Formatter};

pub struct OffsettedVeqDeque<T> {
    pub offset: u64, // Use 64 bits instead of usize to not get into trouble on 32 bits machines.
    pub inner: VecDeque<T>,
}

impl<T> Debug for OffsettedVeqDeque<T>
where
    T: Debug,
{
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), Error> {
        fmt.write_fmt(format_args!("({} skipped) ", self.offset))?;
        fmt.debug_list().entries(self.inner.iter()).finish()
    }
}

impl<T> OffsettedVeqDeque<T> {
    fn usize_to_index(&self, u: usize) -> u64 {
        u as u64 + self.offset
    }

    pub fn with_capacity(capacity: usize) -> Self {
        Self {
            offset: 0,
            inner: VecDeque::with_capacity(capacity),
        }
    }

    pub fn push(&mut self, item: T) {
        self.inner.push_back(item);
    }

    #[inline]
    pub fn get(&self, index: u64) -> Option<&T>
    where
        T: Copy,
    {
        if index < self.offset {
            panic!("index ({}) is before offset ({})", index, self.offset);
        } else {
            self.inner.get((index - self.offset) as usize)
        }
    }

    pub fn len(&self) -> u64 {
        self.usize_to_index(self.inner.len())
    }

    pub fn binary_closest(&self, x: &T) -> u64
    where
        T: Ord + std::fmt::Debug,
    {
        let (first, last) = self.inner.as_slices();
        match first.binary_search(x) {
            Ok(y) => {
                return self.usize_to_index(y);
            }
            Err(y) if y < first.len() => {
                return self.usize_to_index(y - 1);
            }
            _ => {}
        }
        match last.binary_search(x) {
            Ok(y) => self.usize_to_index(y + first.len()),
            Err(y) => self.usize_to_index(y + first.len() - 1),
        }
    }
}

#[cfg(test)]
fn check_binary_closest(offsetted: &OffsettedVeqDeque<i64>) {
    assert_eq!(offsetted.binary_closest(&0), 100);
    assert_eq!(offsetted.binary_closest(&1), 100);
    assert_eq!(offsetted.binary_closest(&2), 100);
    assert_eq!(offsetted.binary_closest(&3), 100);
    assert_eq!(offsetted.binary_closest(&4), 100);
    assert_eq!(offsetted.binary_closest(&5), 101);
    assert_eq!(offsetted.binary_closest(&6), 101);
    assert_eq!(offsetted.binary_closest(&7), 101);
    assert_eq!(offsetted.binary_closest(&8), 101);
    assert_eq!(offsetted.binary_closest(&9), 101);
    assert_eq!(offsetted.binary_closest(&10), 102);
    assert_eq!(offsetted.binary_closest(&11), 102);
    assert_eq!(offsetted.binary_closest(&12), 102);
    assert_eq!(offsetted.binary_closest(&13), 102);
    assert_eq!(offsetted.binary_closest(&14), 102);
    assert_eq!(offsetted.binary_closest(&15), 103);
    assert_eq!(offsetted.binary_closest(&16), 103);
    assert_eq!(offsetted.binary_closest(&17), 103);
    assert_eq!(offsetted.binary_closest(&18), 103);
    assert_eq!(offsetted.binary_closest(&19), 103);
    assert_eq!(offsetted.binary_closest(&20), 103);
    assert_eq!(offsetted.binary_closest(&21), 103);
    assert_eq!(offsetted.binary_closest(&22), 103);
}

#[test]
fn binary_closest_works() {
    let mut offsetted = OffsettedVeqDeque::with_capacity(7);
    offsetted.offset = 100;
    offsetted.push(0);
    offsetted.push(5);
    offsetted.push(10);
    offsetted.push(15);
    assert_eq!(offsetted.inner.as_slices(), (&[0, 5, 10, 15][..], &[][..]));
    check_binary_closest(&offsetted);

    // Do some voodoo magic to make `VecDeque` circle around so that we can test if it still
    // works if `as_slices` returns something different.
    offsetted.inner.push_front(10);
    offsetted.inner.pop_back();
    offsetted.inner.pop_back();
    offsetted.inner.pop_back();
    offsetted.inner.pop_back();
    offsetted.inner.push_front(5);
    offsetted.inner.push_front(0);
    offsetted.inner.push_back(15);
    // End of the voodoo magic.
    assert_eq!(offsetted.inner.as_slices(), (&[0, 5, 10][..], &[15][..]));
    check_binary_closest(&offsetted);

    // Do some voodoo magic to make `VecDeque` circle around so that we can test if it still
    // works if `as_slices` returns something different.
    offsetted.inner.push_back(15);
    offsetted.inner.pop_front();
    offsetted.inner.pop_front();
    offsetted.inner.pop_front();
    offsetted.inner.pop_front();
    offsetted.inner.push_front(10);
    offsetted.inner.push_front(5);
    offsetted.inner.push_front(0);
    // End of the voodoo magic.
    assert_eq!(offsetted.inner.as_slices(), (&[0, 5][..], &[10, 15][..]));
    check_binary_closest(&offsetted);

    // Do some voodoo magic to make `VecDeque` circle around so that we can test if it still
    // works if `as_slices` returns something different.
    offsetted.inner.push_back(15);
    offsetted.inner.pop_front();
    offsetted.inner.pop_front();
    offsetted.inner.pop_front();
    offsetted.inner.pop_front();
    offsetted.inner.push_front(10);
    offsetted.inner.push_front(5);
    offsetted.inner.push_front(0);
    // End of the voodoo magic.
    assert_eq!(offsetted.inner.as_slices(), (&[0][..], &[5, 10, 15][..]));
    check_binary_closest(&offsetted);
}
