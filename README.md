Tdpsola: an implementation of the tdpsola algorithm
===================================================
TD-PSOLA (Time-Domain Pitch-Synchronous Overlap and Add) is an algorithm that allows to modify
the pitch and the speed of a sound independently of each other.

The algorithm
-------------
The idea behind the algorithm on itself is quite simple, but hard to explain.
I have found [this documentation](http://www.speech.zone/td-psola-the-hard-way/) the clearest.

Advantages of TD-PSOLA:
* Preserves the formants, i.e. if you increase the pitch of a human voice, it doesn't sound
like the person has been breathing from a helium balloon
* Performant algorithm for modest pitch/speed changes

Disadvantages of TD-PSOLA:
* Not robust against noise, only suitable for voiced sounds
* Requires a-priori knowledge of the fundamental frequency of the source sound

The implementation
------------------
This is an implementation in the Rust programming language.
Advantages of this implementation:
* `0` samples delay
* separate analysis and synthesis, allowing you to re-use the analysis part
* speed and pitch can be manipulated independently at any time
* many tests
* implemented in a memory-safe programming language, without "unsafe" features

Disadvantages of this implementation:
* The algorithm is extra complicated in order to guarantee `0` samples delay.

Examples
--------
A simple example can be found in the documentation or in the `src/lib.rs` file.
A more worked-out example can be found in the `examples` folder in the source code. 

Planned features
----------------
* suitable for real-time analysis and synthesis
* block-processing for extra speed (can currently only be used sample per sample)

Contributing
------------
We welcome contributions, both in the form of issues and in the form of pull requests. 
Before opening a pull request, please open an issue first so that you know whether a 
subsequent pull request would likely be approved.

If you don’t have a Codeberg account, alternatively, you can contribute via e-mail 
(an email address is in the Cargo.toml file). 
Just creating a Codeberg account is probably the easiest.

Unless explicitly stated otherwise, you agree that your contributions are licensed 
as described below.

License
-------
This is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This software is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

The full license text can be found in the file 
[LICENSE-AGPL-3.0.md](LICENSE-AGPL-3.0.md)
that should be distributed with this source code. 
If it is not there, see <https://www.gnu.org/licenses/>.